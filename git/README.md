> # git and more

---

> ### Table of Content

- [Git Workflow explained](#git-workflow-explained)
- [Rebase vs Merge](#rebase-vs-merge)
- [Submodules and Subtrees](#submodules-and-subtrees)
- [Some useful commands](#some-useful-commands)
- [Bitbucket](#bitbucket)
  - [Set Bitbucket repositories for personal use and for work use](#set-bitbucket-repositories-for-personal-use-and-for-work-use)
  - [Other Bitbucket services](#other-bitbucket-services)
- [Other Links](#other-links)
- [ToDo:](#todo)

---

# Git Workflow explained

This is the workflow I'm trying to use when possible :-)  
[A successful Git branching model, By Vincent Driessen](https://nvie.com/posts/a-successful-git-branching-model/)

[Another workflow by Emma Hogbin Westby (Oreilly)](https://www.oreilly.com/learning/workflows-that-work)

[Another workflow by Ryan Glover (Meteor-Chef)](https://themeteorchef.com/blog/defining-a-simple-git-workflow)

# Rebase vs Merge

[Getting solid at Git rebase vs. merge, by Christophe Porteneuve
(Medium)](https://medium.com/@porteneuve/getting-solid-at-git-rebase-vs-merge-4fa1a48c53aa)

# Submodules and Subtrees

- [Mastering Git subtrees, by Christophe Porteneuve
  (Medium)](https://medium.com/@porteneuve/mastering-git-subtrees-943d29a798ec)
- [Mastering Git submodules, by Christophe Porteneuve
  (Medium)](https://medium.com/@porteneuve/mastering-git-submodules-34c65e940407)
- [Working with submodules, GitHub Blog, jaw6](https://blog.github.com/2016-02-01-working-with-submodules/)
- [Git Submodules: Adding, Using, Removing, Updating. By Chris Jean](https://chrisjean.com/git-submodules-adding-using-removing-and-updating/)
- [Git submodules: core concept, workflows, and tips - Atlassian Blog](https://www.atlassian.com/blog/git/git-submodules-workflows-tips)

# Some useful commands

- `git merge --no-ff myfeature`
- [lot of interesting commands inside sh script](https://github.com/nvie/git-toolbelt#readme)
- [30 Git CLI options you should know about, by Christophe Porteneuve
  (Medium)](https://medium.com/@porteneuve/30-git-cli-options-you-should-know-about-15423e8771df)
- [Mastering Git Reset: Commit Alchemy, by Christophe Porteneuve
  (Medium)](https://medium.com/@porteneuve/mastering-git-reset-commit-alchemy-ba3a83bdfddc)
- [Basic Git commands - Atlassian Documentation](https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html)

# Bitbucket

## Set Bitbucket repositories for personal use and for work use

(This is for Mac. Windows and Linux need some modifications)  
Some links with more information:

- [Fredrik Andersson from Medium (this was the major source to my workflow)](https://medium.com/@fredrikanderzon/setting-up-ssh-keys-for-multiple-bitbucket-github-accounts-a5244c28c0ac)
- [Bitbucket Support: Set up additional SSH keys](https://confluence.atlassian.com/bitbucket/set-up-additional-ssh-keys-271943168.html)
- [Bitbucket Support: Change the remote URL to your repository](https://confluence.atlassian.com/bitbucket/change-the-remote-url-to-your-repository-794212774.html)
- [From Stack Overflow](https://stackoverflow.com/questions/21139926/how-to-maintain-multiple-bitbucket-accounts-with-multiple-ssh-keys-in-the-same-s)
- [Another article](https://developer.atlassian.com/blog/2016/04/different-ssh-keys-multiple-bitbucket-accounts/)

My workflow:

1. Create private/default ssh key
   ```
   ssh-keygen -t rsa
   ssh-add ~/.ssh/id_rsa
   ```
2. Create work/company ssh key
   ```
   ssh-keygen -t rsa -C "companyName" -f "companyName"
   ssh-add ~/.ssh/companyName
   ```
3. Edit `~/.ssh/config`
   ```
   Host bitbucket.org
     HostName bitbucket.org
     IdentityFile ~/.ssh/id_rsa
   Host companyname.bitbucket.org
     HostName bitbucket.org
     IdentityFile ~/.ssh/companyName
   ```
4. Add ssh keys to Bitbucket  
   On Bitbucket site, under the user settings we should paste our _PUBLIC_ keys.  
   This should be done for each account.
   - For the personal account: `$ pbcopy < ~/.ssh/id_rsa.pub`, then paste it
   - For the work/company account: `$ pbcopy < ~/.ssh/companyName.pub`, then paste it
5. Work with the repositories
   - Clone private repository:  
     `git clone git@bitbucket.org:<Personal_user_name>/<repository_name>.git`
   - Clone Company repository:  
     In company repository, we will use the alias from the `~/.ssh/config` file instead of the bitbucket url.  
     `git clone git@companyname.bitbucket.org:<work_user_name>/<repository_name>.git`
   - Clone Company team repository:  
     Company repository can be under a team.  
     In such case, we need to use the team name and not our company user name.  
     `git clone git@companyname.bitbucket.org:<group_name>/<repository_name>.git`
   - Change existing git origin to ssh:  
     `git remote set-url origin git@companyname.bitbucket.org:<group_name>/<repository_name>.git`
   - Verify:
     `git fetch -v`

## Other Bitbucket services

- [Issue tracker](https://confluence.atlassian.com/bitbucket/issue-trackers-221449750.html)
- [Web Hooks](https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html)
- [Wikis](https://confluence.atlassian.com/bitbucket/wikis-221449748.html)

# Other Links

- [Interesting git project: git-extras](https://github.com/tj/git-extras)
- [Another git project: Git power tools](https://nvie.com/posts/git-power-tools/)  
  (In the repository, there are few sh scripts with lot of tricks)

# ToDo:

- Pull request
- merge conflicts
- mistakes, problems solving, recovery
- GUI Clients
- Hooks
- Add more links

# Programming and More

As most of us, I found myself with lot of `notes, links and bookmarks`.  
Each exists in a different place, using different technology.

This **_(on-going) project_** is my attempt on "organizing" some of them into structured data.  
Each subject will have a folder, which will include `Markdown files`.  
Each file with some links, insights, tips, tricks and info regarding the subject of the folder.

All files are just reminders to help me organizing specific technical data, information and documentation.  
I hope that it will help some other people as well.

Best Regards,  
Izack

# Markdown files:

- [`git` tips, workflow and more](git/README.md)
- `Dockers`
  - [`Dockers` on `AWS` using Fargate](Dockers/aws.md)
  - [`Dockers` build for `meteor`](Dockers/meteor.md)
  - [`Dockers` - some (very) basic data](Dockers/dockers.md)

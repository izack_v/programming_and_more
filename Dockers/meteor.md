# Meteor to AWS:

[Step-by-step guide to deploy a Meteor Application to AWS Elastic Beanstalk in Production](https://blog.416serg.me/step-by-step-guide-to-deploy-a-meteor-application-to-aws-elastic-beanstalk-in-production-1bd899bbf3f9)

# Galaxy/Meteor

- [Deployment and Monitoring](https://guide.meteor.com/deployment.html#docker)
- [Build system](https://guide.meteor.com/v1.2/build-tool.html)
- [build-app.sh](https://github.com/meteor/galaxy-images/blob/master/build-app.sh)

# Heroku

[Meteor build + Heroku recipe](https://forums.meteor.com/t/meteor-build-heroku-recipe/37793)

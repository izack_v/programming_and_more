# Fargate

- [AWS Fargate: A Product Overview](https://aws.amazon.com/blogs/compute/aws-fargate-a-product-overview/)
- [A Tutorial on AWS Fargate](https://medium.com/@chasdevs/a-tutorial-on-aws-fargate-bd63828b6d6c)
- [Repository for the tutorial](https://github.com/Footage-Firm/learn-fargate)
- [Getting Started with Amazon ECS using Fargate](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted.html)
- [Tutorial: Creating a Cluster with a Fargate Task Using the AWS CLI](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_AWSCLI_Fargate.html)

# Workflow

- [How I do local Docker development for my AWS Fargate application](https://medium.com/containers-on-aws/how-i-do-local-docker-development-for-my-aws-fargate-application-8957e3fdb50)
- [Running a Docker Environment Locally with the EB CLI](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create_deploy_docker-eblocal.html)
- [Monitor Website Latency using CloudWatch Logs CLI Plugin](https://aws.amazon.com/blogs/devops/monitor-website-latency-using-cloudwatch-logs-cli-plugin/)

# Build and Deploy

- [Step-by-step guide to deploy a Meteor Application to AWS Elastic Beanstalk in Production](https://blog.416serg.me/step-by-step-guide-to-deploy-a-meteor-application-to-aws-elastic-beanstalk-in-production-1bd899bbf3f9)
- [Deploying a Node.js app to AWS Elastic Beanstalk](https://medium.com/@xoor/deploying-a-node-js-app-to-aws-elastic-beanstalk-681fa88bac53)
- [Deploying a Docker Container to AWS with Elastic Beanstalk](https://medium.com/@sommershurbaji/deploying-a-docker-container-to-aws-with-elastic-beanstalk-28adfd6e7e95)
- [Building, deploying, and operating containerized applications with AWS Fargate](https://aws.amazon.com/blogs/compute/building-deploying-and-operating-containerized-applications-with-aws-fargate/)
- [Task Networking in AWS Fargate](https://aws.amazon.com/blogs/compute/task-networking-in-aws-fargate/)

# ELB

- [Tutorials for Application Load Balancers](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/application-load-balancer-tutorials.html)
- [Limits](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-limits.html)
- [Config idle timeout](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/config-idle-timeout.html)
- [Troubleshoot Your Application Load Balancers](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-troubleshooting.html)

# Parameter Store

[AWS Systems Manager Parameter Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html)

# S3

[Download full bucket](https://stackoverflow.com/questions/8659382/downloading-an-entire-s3-bucket)
